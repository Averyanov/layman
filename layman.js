const PSD = require('psd');
const fs = require('fs');
const f = {'out' : { 'css' : 'out/s.css', 'html' : 'out/i.html' } };
const commandLineArgs = require('command-line-args');
const options = commandLineArgs([{ name: 'maket', alias: 'm', type: String }]);

function ec(err){return !err || console.log(err)};
function renderSlide(c){fs.appendFile(f.out.html, parseNodeList(c), ec);};
function parseNodeList(c){return c ? c.reduce(wrapNode.bind(c), '') : '';};
function wrapNode(p,c,i,a){
	let attr = {};
	let style;

	[attr, style] = getNodeProp(c);

	fs.appendFile(f.out.css, `#${attr.id}{${style}}`, ec);

	return p + `<${attr.tagN} id='${attr.id}'>\r${parseNodeList(c.children())}\r</${attr.tagN}>\r`;
}
function getNodeProp(c)
{	
	let sss = Object.keys(c.export()).reduce(function(p,c,i,a){
		propScenario[c].call(p, c, this.export()[c], this);

		return p;
	}.bind(c), {attr: {},style: ''});

	return [sss.attr, sss.style];
}
function genUniqSelect(v){return v.replace(/[^A-Za-z]/g, '_') + Math.floor(Math.random() * 10000)};
function ignore(k,v){return this};
function setUnit(k,v){this.style += `${k}:${v}` + (v ? 'px' : '') + ';'};
function pPos(k, v, lo){setUnit.call(this, k, v - lo.parent[k]);}; // просто присвоїти було б достатньо, але...
function setBg(k,v){this.style += 'background: url();'};
function pType(k, v, lo){this.attr.tagN = v === 'group' ? 'co-container' : false};
function pVisible(k, v, lo){this.style += v === 'hidden' ? 'visibility:hidden;' : ''};
function pOpacity(k, v, lo){this.style += v !== 1 ? `opacity:${v};` : ''};
function pName(k, v, lo){this.attr.id = ''+genUniqSelect(v)};
function pText(k, v, lo){this.attr.tagN = 'co-text'};
let propScenario = { // тут можна було б сетери заюзать, але мені вже впладло (строка 23)
	type : pType,	
	visible : pVisible,
	opacity : pOpacity,
	blendingMode : ignore,
	name : pName,
	left : pPos,
	right : ignore,
	top : pPos,
	bottom : ignore,
	height : setUnit,
	width : setUnit,
	mask : ignore,
	text : pText,
	image : setBg,
	children : ignore
};

let psd = PSD.fromFile(options.maket);
psd.parse();
let s = psd.tree();
let c = s.children();
fs.unlink(f.out.css, ec);
fs.unlink(f.out.html, ec);
renderSlide(c); 