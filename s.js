// path to designe file
const p_df = process.argv[2];

const PSD = require('psd');
const fs = require('fs');
const f = {'out' : 
	{ 
		'css' : 'out/s.css', 
		'html' : 'out/i.html',
		'dmodel' : 'out/dmodel.js', // data model
		'lmodel' : 'out/lmodel.js'	// local(translate) model
	} 
};

function ec(err){return !err || console.log(err)};

var s = PSD.fromFile(p_df);

// return;

s.parse();

var t = s.tree().export(), d = t.document, c = t.children;

fs.unlink(f.out.css, ec);
fs.unlink(f.out.html, ec);

var genUniqSelect = function(v){return v.replace(/[^A-Za-z]/g, '_') + Math.floor(Math.random() * 10000)}

var ignore = function(k,v){return this};

var setUnit = function(k,v){this.style += `${k}:${v}` + (v ? 'px' : '') + ';'};

var pLeft = function(k, v, lo){
	var offset = s.tree().childrenAtPath(lo.name)[0].parent.left; 
	var lPos = v - offset
	this.style += `left:${lPos}px;`;
}

var pTop = function(k, v, lo){
	var offset = s.tree().childrenAtPath(lo.name)[0].parent.top; 
	var tPos = v - offset;
	this.style += `top:${tPos}px;`;
};

var setBg = function(k,v){this.style += 'background: url();'};

var pType = function(k, v, lo){this.attr.tagN = v === 'group' ? 'co-container' : false}

var pVisible = function(k, v, lo){this.style += v === 'hidden' ? 'visibility:hidden;' : ''}

var pOpacity = function(k, v, lo){this.style += v !== 1 ? `opacity:${v};` : ''}

var pName = function(k, v, lo){this.attr.id = ''+genUniqSelect(v)};

var pText = function(k, v, lo){this.attr.tagN = 'co-text'}

var propScenario = {
	type : pType,	
	visible : pVisible,
	opacity : pOpacity,
	blendingMode : ignore,
	name : pName,
	left : pLeft,
	right : ignore,
	top : pTop,
	bottom : ignore,
	height : setUnit,
	width : setUnit,
	mask : ignore,
	text : pText,
	image : setBg,
	children : ignore
}

var getLayerProp = function(c)
{
	var initProp = {
		attr: {},
		style: '',
		model: {}
	}
	
	var sss = Object.keys(c).reduce(function(p,c,i,a){
		propScenario[c].call(p, c, this[c], this);

		return p;
	}.bind(c), initProp);

	return [sss.attr, sss.style, sss.model];
}

var wrapTag = function(p,c,i,a){
	// console.log(` Get tag(#${counter++}) \r`)

	var attr = {};
	var style;
	var model = {};

	[attr, style, model] = getLayerProp(c);

	fs.appendFile(f.out.css, `#${attr.id}{${style}}`, ec);

	return p + `<${attr.tagN} id='${attr.id}'>\r${renderHtml(c.children)}\r</${attr.tagN}>\r`;
}

var renderHtml = function(c){
	return c ? c.reduce(wrapTag.bind(c), '') : '';
}

var renderSlide = function(c){
	var html = renderHtml(c);

	fs.appendFile(f.out.html, html, ec);
}

renderSlide(c);























// 1. render html

	// if(c.children) return `<container id='${c.name}'> \r  </container> \r`
	// else return `<container
// 2. get image




//096 096 37 14 Kedr <=> space_SHIP_

// var commonHeaders = {'Content-Type': 'text/html'};
// var cssHeaders = {'Content-Type': 'text/css'};

// var server = http.createServer(function (req, resp) {
//     // resp.writeHead(200, { 'Content-Type': 'text/html' });
// 	if (req.url.indexOf('.css') != -1) {
// 	    resp.setHeader("Content-Type", 'text/css');
// 	    resp.write('body{border: 1px solid red}');
// 	    resp.end();
// 	}
// 	else {
// 		resp.writeHead(200, {'Content-Type': 'text/html'});
// 		resp.write("<link rel='stylesheet' href='/s.css' />");
// 		resp.end();
// 	}
//     resp.end();

// });

// server.listen(5050);
 
// console.log('Server Started listening on 5050');